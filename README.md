# ORB Project (Creating 3D picture of people with RPi)
========================================================================

![Diagram](assets/images/stitch.png)

## Install dependencies

### Install Kivy 1.9.2dev on Ubuntu 16.04 LTE

    sudo apt-get update
    sudo apt-get install build-essential git
    sudo pip install pygments docutils

    sudo apt-get install python-setuptools python-pygame python-opengl python-gst0.10 python-enchant gstreamer0.10-plugins-good python-dev
    sudo apt-get install build-essential libgl1-mesa-dev libgles2-mesa-dev zlib1g-dev python-pip

    sudo pip install --upgrade Cython==0.23

    git clone git://github.com/kivy/kivy.git
    cd kivy
    python setup.py build_ext --inplace -f
