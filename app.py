import os

# Import this before App import
import configure_before

from kivy.app import App

# Import configure after App import
import configure

from kivy.base import ExceptionHandler
from kivy.base import ExceptionManager
import util.common
import util.net
import util.time_util
from kivy.config import _is_rpi
from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty, NumericProperty, ObjectProperty
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.screenmanager import NoTransition, SlideTransition
from kivy.clock import Clock
import widgets.widgetmanager
from screens.screenmanager import screens
from kivymd.theming import ThemeManager
import settings_orb

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'
# Check log dir
if not os.path.exists(cur_dir + 'log'):
    os.mkdir(cur_dir + 'log')


Builder.load_file(os.path.join(os.path.dirname(__file__), 'app.kv'))


# class ORBExceptionHandler(ExceptionHandler):
#
#     error_dlg = None
#
#     def __init__(self):
#         super(ORBExceptionHandler, self).__init__()
#         self.error_dlg = NotificationDialog(title='Error')
#
#     def handle_exception(self, exception):
#         print '-- Exception: {}'.format(repr(exception))
#         self.error_dlg.message = repr(exception)
#         app.save_exception(exception)
#         self.error_dlg.open()
#         return ExceptionManager.PASS
#
#
# ExceptionManager.add_handler(ORBExceptionHandler())


class MainWidget(FloatLayout):

    def __init__(self, **kwargs):
        super(MainWidget, self).__init__(**kwargs)


class ORBApp(App):

    cur_screen = StringProperty('')
    show_back_button = BooleanProperty(False)
    cur_screen_inst = ObjectProperty(None)
    theme_cls = ThemeManager()
    exception = None

    def build(self):
        """
        base function of kivy app
        :return:
        """
        self.title = util.common.get_title()
        self.root = MainWidget()
        self.start_schedules()
        self.switch_screen(settings_orb.INITIAL_SCREEN)

    def start_schedules(self):
        Clock.schedule_interval(self.show_time, 1)

    def switch_screen(self, screen_name, direction=None, **kwargs):
        """
        Go to given screen
        :param screen_name:     destination screen name
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        sm = self.root.ids.sm
        sm.transition = NoTransition() if direction is None else SlideTransition()
        if screen_name in screens.keys() and self.cur_screen != screen_name:
            screen = screens[screen_name](name=screen_name)
            self.cur_screen_inst = screen
            if direction is None:
                sm.switch_to(screen)
            else:
                sm.switch_to(screen, direction=direction)
            # logger.debug('Moving to {} in direction of {}'.format(screen_name, direction))
            self.cur_screen = screen_name
            self.root.ids.toolbar.title = 'ORB - {}'.format(screen.title)
            return True

    # ================================= Scheduled functions ======================================

    def show_time(self, *args):
        local_time = util.time_util.get_local_time()
        self.root.ids.lb_time.text = local_time.strftime("%B %d, %Y  %I:%M:%S %p")

    def save_exception(self, ex):
        self.exception = ex

    def get_exception(self):
        return self.exception


if __name__ == '__main__':

    if _is_rpi:
        if os.geteuid() != 0:
            msg = "You need to have root privileges to run this script.\nPlease try again by using 'sudo'.\nExiting..."
            exit(msg)
        os.system('chmod 777 /etc/network/interfaces')

    app = ORBApp()
    app.run()
