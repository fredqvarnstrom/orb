"""
This file defines global ui variables such as colors,
font size for different labels, etc.
"""

COLOR_1 = [1, 1, 1, 1]
COLOR_2 = [0.13, 0.59, 0.95, 1]
BACKGROUND_COLOR_DARK = [.12, .12, .12, 1]
BACKGROUND_COLOR_LIGHT = [.12, .32, .4, 1]
BACKGROUND_COLOR_LIGHT_LIGHT = [.12, .32, .4, .8]
DISABLED_COLOR = [0.62, 0.68, 0.71, 0.9]
ACCENT_COLOR = [1, .6, .2, 1]

H0_FONT_SIZE = 72

H1_FONT_SIZE = 48

H2_FONT_SIZE = 36

H3_FONT_SIZE = 28

H4_FONT_SIZE = 24

H5_FONT_SIZE = 20

H6_FONT_SIZE = 17

P_FONT_SIZE = 17

MODAL_BACKGROUND = [0, .05, .05, .5]
