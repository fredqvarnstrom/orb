import os

from kivy.clock import Clock
from kivy.config import Config
from kivy.lang import Builder
from loggers.handlers import configure_loggers
import settings_orb

Config.read(settings_orb.INI_FILE)
Config.set('graphics', 'width', str(settings_orb.WINDOW_WIDTH))
Config.set('graphics', 'height', str(settings_orb.WINDOW_HEIGHT))
Config.set('graphics', 'fullscreen', 'yes' if settings_orb.WINDOW_FULLSCREEN else 'no')

configure_loggers()

# .set('kivy', 'keyboard_mode', 'systemanddock')
Clock.max_iteration = 20

# Override some default widgets + define new ones
Builder.load_file(os.path.join(os.path.dirname(__file__), 'widgets', 'kv', 'widgets.kv'))
