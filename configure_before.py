# Do not import anything from kivy here
import ConfigParser
import os
import settings_orb

# used to turn off default kivy loggers
os.environ["KIVY_NO_FILELOG"] = '1'
os.environ['KIVY_USE_DEFAULTCONFIG'] = '1'


config = ConfigParser.RawConfigParser()
conf_file = settings_orb.INI_FILE

config.read(conf_file)

is_exist = True

try:
    config.get('general', 'debug')
except (ConfigParser.NoOptionError, ConfigParser.NoSectionError):
    is_exist = False

if not is_exist:
    cfgfile = open(settings_orb.INI_FILE, 'w')
    config.add_section('general')
    config.set('general', 'title', 'ORB')
    config.set('general', 'timezone', 'Europe/Amsterdam')
    config.set('general', 'debug', 'false')
    config.add_section('orb')
    config.set('orb', 'save_path', os.path.expanduser('~/'))
    config.write(cfgfile)
    cfgfile.close()
    print 'New Configuration File is created.'
