from datetime import datetime
import logging
from os.path import expanduser
from time import strftime
import os


class ORBFileHandler(logging.Handler):
    '''
    Fork from FileHandler in kivy.logger
    '''
    filename = ''
    fd = None
    max_days = 15
    log_file = 'orb_%y-%m-%d_%_.txt'
    log_dir = expanduser('~/.orb_logs')

    def purge_logs(self):
        directory = self.log_dir
        join = os.path.join
        unlink = os.unlink

        # search all log files
        l = [join(directory, x) for x in os.listdir(directory)]

        # get creation time on every files
        l = [{'fn': x, 'ctime': datetime.fromtimestamp(os.path.getctime(x))} for x in l]

        # filter by creation date
        today = datetime.now()
        l = (item for item in l if (today - item['ctime']).days > self.max_days)

        # now, unlink every files in the list
        for filename in l:
            unlink(filename['fn'])

    def _configure(self, *largs, **kwargs):

        log_name = self.log_file

        if not os.path.exists(self.log_dir):
            os.makedirs(self.log_dir)

        self.purge_logs()

        pattern = log_name.replace('%_', '@@NUMBER@@')
        pattern = os.path.join(self.log_dir, strftime(pattern))
        n = 0
        while True:
            filename = pattern.replace('@@NUMBER@@', str(n))
            if not os.path.exists(filename):
                break
            n += 1
            if n > 10000:  # prevent maybe flooding ?
                raise Exception('Too many logfile, remove them')

        if self.filename == filename and self.fd is not None:
            return
        self.filename = filename
        if self.fd is not None:
            self.fd.close()
        self.fd = open(filename, 'w')

    def _write_message(self, record):
        if self.fd in (None, False):
            return

        self.fd.write('[%-18s] ' % record.levelname)
        self.fd.write(record.msg)
        self.fd.write('\n')
        self.fd.flush()

    # def emit_history(self):
    #     while LoggerHistory.history:
    #         _message = LoggerHistory.history.pop()
    #         self._write_message(_message)

    def emit(self, message):
        if self.fd is None:
            self._configure()
        self._write_message(message)
