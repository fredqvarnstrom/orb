import os
import threading
import time
from kivy.lang import Builder
from screens.base.base import BaseScreen
from kivy.clock import mainthread

Builder.load_file(os.path.join(os.path.dirname(__file__), 'menu.kv'))


class MenuScreen(BaseScreen):

    def __init__(self, **kwargs):
        super(MenuScreen, self).__init__(**kwargs)

    def on_enter(self, *args):
        super(MenuScreen, self).on_enter(*args)

    def on_shoot(self):
        self.switch_screen('result_screen', 'up')
