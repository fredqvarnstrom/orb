from kivy.uix.screenmanager import ScreenManager, NoTransition
from screens.calibration.screen import CalibrationScreen
from screens.menu.screen import MenuScreen
from screens.result.screen import ResultScreen
from screens.settings.screen import SettingsScreen
from screens.weather.screen import WeatherScreen
from screens.welcome.screen import WelcomeScreen

sm = ScreenManager(transition=NoTransition())

screens = {
    'welcome_screen': WelcomeScreen,
    'menu_screen': MenuScreen,
    'settings_screen': SettingsScreen,
    'result_screen': ResultScreen,
    'calibration_screen': CalibrationScreen,
    'weather_screen': WeatherScreen,
}
