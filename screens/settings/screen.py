import os
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.screenmanager import NoTransition, SlideTransition
from screens.base.base import BaseScreen
from kivy.clock import Clock

from screens.settings.tab_manager import settings_tabs

Builder.load_file(os.path.join(os.path.dirname(__file__), 'settings.kv'))


class SettingsScreen(BaseScreen):

    cur_tab = None
    show_back_button = False
    tab_num = 0
    start_tab = StringProperty('general')

    def __init__(self, **kwargs):
        super(SettingsScreen, self).__init__(**kwargs)
        Clock.schedule_once(self.initialize)

    def on_enter(self, *args):
        if self.cur_tab is not None:
            self.cur_tab.on_enter()
        super(SettingsScreen, self).on_enter(*args)

    def initialize(self, *args):
        self.switch_tab(self.start_tab)

    def switch_tab(self, tab_name, tab_btn=None):
        """
        Go to given screen
        :param tab_btn: Tab button widget
        :param tab_name:     destination tab name
        :return:
        """
        sm = self.ids.sm
        tab = settings_tabs[tab_name](screen_widget=self)
        if tab_btn is None:
            tab_btn = self.ids['btn_{}'.format(tab_name)]

        if self.cur_tab is None or tab_btn is None:
            sm.transition = NoTransition()
        else:
            sm.transition = SlideTransition()

        if self.cur_tab is None:
            sm.switch_to(tab)
            self.cur_tab = tab
            tab_btn.on_tabbed(True)
        elif self.cur_tab.name != tab.name:
            direction = 'left' if self.cur_tab.index < tab.index else 'right'
            sm.switch_to(tab, direction=direction)
            self.ids['btn_{}'.format(self.cur_tab.name)].on_tabbed(False)
            self.cur_tab = tab
            tab_btn.on_tabbed(True)

    def on_leave(self, *args):
        self.cur_tab.on_leave()
        super(SettingsScreen, self).on_leave(*args)
