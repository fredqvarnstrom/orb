from kivy.uix.screenmanager import ScreenManager, NoTransition
from screens.settings.tabs.date_time import DateTimeTab
from screens.settings.tabs.ethernet import EthernetTab
from screens.settings.tabs.general import GeneralTab
from screens.settings.tabs.upgrade import UpgradeTab
from screens.settings.tabs.wifi import WiFiTab


sm_settings = ScreenManager(transition=NoTransition())

settings_tabs = {
    'general': GeneralTab,
    'ethernet': EthernetTab,
    'wifi': WiFiTab,
    'datetime': DateTimeTab,
    'upgrade': UpgradeTab,
}
