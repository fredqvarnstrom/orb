import os
from kivymd.snackbar import Snackbar
import util.common
from kivy.lang import Builder
from screens.settings.tabs.base import BaseTab


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'general.kv'))


class GeneralTab(BaseTab):

    index = 0

    def __init__(self, **kwargs):
        super(GeneralTab, self).__init__(**kwargs)

    def on_enter_tab(self, *args):
        self.ids.version.text = util.common.get_current_version()
        self.ids.name.text = util.common.get_name()
        self.ids.hostname.text = util.common.get_hostname()

    def on_leave(self, *args):
        pass

    def on_apply(self):
        new_name = self.ids.name.text
        if len(new_name) == 0:
            Snackbar(text='Invalid Name.', background_color=(.8, 0, .3, .5)).show()
            return
        util.common.set_name(new_name)
        Snackbar(text='Device Name is updated.').show()
