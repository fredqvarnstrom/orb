import os

import util.common
from kivy.lang import Builder
from kivy.properties import StringProperty

from screens.settings.tabs.base import BaseTab

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'upgrade.kv'))


class UpgradeTab(BaseTab):

    current_version = StringProperty('')
    new_version = StringProperty('')
    index = 4

    def __init__(self, **kwargs):
        super(UpgradeTab, self).__init__(**kwargs)
        self.current_version = util.common.get_current_version()
        self.new_version = util.common.get_new_version()

    def on_upgrade(self):
        pass

    def on_enter_tab(self, *args):
        pass
