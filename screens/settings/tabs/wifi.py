import os
import threading
from functools import partial
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.widget import Widget
from kivymd.demo import AvatarSampleWidget
from kivymd.list import TwoLineAvatarIconListItem
from screens.settings.tabs.base import BaseTab
from kivy.clock import Clock, mainthread
import util.net
from widgets.dialog import WiFiInfoDialog, WiFiConnectDialog
from widgets.wifi.wifi_item import MDWiFiItem

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'wifi.kv'))


class WiFiTab(BaseTab):

    index = 2
    cur_ssid = StringProperty('')

    def __init__(self, **kwargs):
        super(WiFiTab, self).__init__(**kwargs)

    def on_enter_tab(self, *args):
        threading.Thread(target=self.get_ap_list).start()

    def get_ap_list(self, *args):
        ap_list = util.net.get_ap_list('wlan0')
        cur_ap_name = util.net.get_current_ap()

        # Move current AP to the beginning of the list
        if cur_ap_name is not None:
            cur_ap = (ap for ap in ap_list if ap['ssid'] == cur_ap_name).next()
            ap_list.remove(cur_ap)
        else:
            cur_ap = None

        ap_list = sorted(ap_list, key=lambda k: k['quality'])
        ap_list.reverse()

        if cur_ap_name is not None:
            ap_list = [cur_ap, ] + ap_list
            self.cur_ssid = cur_ap_name
        Clock.schedule_once(partial(self.update_ap_list, cur_ap_name, ap_list))

    @mainthread
    def update_ap_list(self, cur_ap_name, ap_list, *args):
        self.ids.ml_left.clear_widgets()
        self.ids.ml_right.clear_widgets()
        for i in range(len(ap_list)):
            ap = ap_list[i]
            img_index = ap['quality'] / 20
            if img_index == 5:
                img_index = 4

            txt_ssid = ap['ssid']
            if txt_ssid == cur_ap_name:
                txt_ssid += '   -   Connected'
            try:
                item = MDWiFiItem(type='two-line', text=txt_ssid,
                                  secondary_text=ap['address'] + (' (encrypted)' if ap['encrypted'] else ''))
                item.add_widget(AvatarSampleWidget(source='assets/images/wifi/{}.png'.format(img_index)))
                item.bind(on_release=partial(self.on_btn, ap['ssid']))
                if i % 2 == 0:
                    self.ids.ml_left.add_widget(item)
                    self.ids.ml_left.parent.height = self.ids.ml_left.height
                else:
                    self.ids.ml_right.add_widget(item)
            except ValueError:
                pass
        if len(ap_list) % 2 == 1:
            self.ids.ml_right.add_widget(Widget(size_hint_y=None, height=MDWiFiItem().height))
        self.ids.ly_root.remove_widget(self.ids.ly_loading)

    def on_btn(self, ssid, *args):
        if self.cur_ssid == ssid:
            WiFiInfoDialog().open()
        else:
            popup = WiFiConnectDialog(ssid=ssid)
            popup.bind(on_done=self.on_perform_connect)
            popup.open()

    def on_perform_connect(self, *args):
        result_ip = args[1]
        print 'New IP: {}'.format(result_ip)
        Clock.schedule_once(self.update_ap_list)
