import os
import threading
from functools import partial
from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from screens.base.base import BaseScreen
from util.weather import get_weather

Builder.load_file(os.path.join(os.path.dirname(__file__), 'weather.kv'))


class WeatherScreen(BaseScreen):

    def on_enter(self, *args):
        """
        Clear all previously used data
        :param args:
        :return:
        """
        super(WeatherScreen, self).on_enter(*args)
        self.start_polling_weather()
        Clock.schedule_interval(self.start_polling_weather, 15 * 60)    # Update weather every 15 min

    def start_polling_weather(self, *args):
        self.ids.loading_indicator.opacity = 1
        self.ids.card_fail.opacity = 0
        threading.Thread(target=self.poll_weather_data).start()

    def poll_weather_data(self, *args):
        weather_data = get_weather()
        if weather_data is None:
            Clock.schedule_once(self.on_error_weather_data)
        else:
            Clock.schedule_once(partial(self.on_success_weather_data, weather_data))

    @mainthread
    def on_error_weather_data(self, *args):
        self.ids.loading_indicator.opacity = 0
        if self.ids.card_weather.opacity == 0:
            self.ids.card_fail.opacity = 1

    @mainthread
    def on_success_weather_data(self, d, *args):
        self.ids.loading_indicator.opacity = 0
        for _key in self.ids.keys():
            if _key in d.keys():
                self.ids[_key].text = d[_key]

        self.ids.img_state_1.source = 'assets/images/weather/{}.png'.format(d['state'].lower())
        self.ids.img_state_2.source = 'assets/images/weather/{}.png'.format(d['state'].lower())
        self.remove_widget(self.ids.loading_indicator)
        self.ids.card_weather.opacity = 1
        self.ids.card_detail.opacity = 1
        # print 'Successfully get weather data: {}'.format(d)

    def on_leave(self, *args):
        Clock.unschedule(self.start_polling_weather)
