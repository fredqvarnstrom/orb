import os
from kivy.lang import Builder
from screens.base.base import BaseScreen

Builder.load_file(os.path.join(os.path.dirname(__file__), 'welcome.kv'))


class WelcomeScreen(BaseScreen):

    def on_touch_down(self, touch):
        super(WelcomeScreen, self).on_touch_down(touch)
        self.switch_screen('menu_screen')
