import platform

import numpy as np
import imutils
import cv2
import math

import time


class Stitcher:

    def __init__(self, width_thresh_ratio=10.0, height_thresh_ratio=50.0):
        """
        :param width_thresh_ratio: Ratio of width threshold in percentage
        :param height_thresh_ratio: Ratio of height threshold in percentage
        """
        # determine if we are using OpenCV v3.X
        self.isv3 = imutils.is_cv3()

        self.height_a = 0.0
        self.width = 0.0
        self.height_b = 0.0

        self.width_thresh_ratio = width_thresh_ratio
        self.height_thresh_ratio = height_thresh_ratio

    def stitch(self, images, show_matches=False, b_tuning=True, ratio=.9):
        """
        Perform stitch process.
        If fixed ratio is used, just call this function with b_tunning=False
            stitch(images, b_tuning=False, ratio=.5)
        Otherwise, we do not need ratio value since it will be obtained in itself.
            stitch(images)
        :param ratio:
        :param b_tuning:
        :param images:
        :param show_matches:
        :return:
        """
        if b_tuning:
            step = .05
            error_result = 10000
            _ratio = .95
            while True:
                (_result, _vis, err) = self.stitch(images, b_tuning=False, ratio=_ratio)
                print 'error: {}  ration: {}'.format(err, _ratio)
                if err < error_result:
                    _ratio -= step
                    error_result = err
                else:
                    _ratio += step      # Previous ratio value is the final value.
                    print 'Optimized ratio: ', _ratio
                    break
            # print 'Stitching {} & {}'.format(images[0], images[1])
            return self.stitch(images, b_tuning=False, ratio=_ratio, show_matches=show_matches)

        # unpack the images, then detect keypoints and extract
        # local invariant descriptors from them
        (image_b, image_a) = images
        (kps_a, features_a) = self.detect_and_describe(image_a)
        (kps_b, features_b) = self.detect_and_describe(image_b)

        (self.height_b, self.height_a, self.width) = (image_b.shape[0], image_a.shape[0], image_a.shape[1])

        # match features between the two images
        m = self.match_keypoints_ransac(kps_a, kps_b, features_a, features_b, ratio=ratio)

        # if the match is None, then there aren't enough matched
        # keypoints to create a panorama
        if m is None:
            return None

        # otherwise, apply a perspective warp to stitch the images
        # together
        (matches, h, status, err) = m

        # creating a stitched image as result
        h_a, w_a = image_a.shape[:2]
        h_b, w_b = image_b.shape[:2]

        new_h_a, new_w_a = 0, 0
        new_h_b, new_w_b = 0, 0

        true_matches = 0
        # loop over the matches
        for ((trainIdx, queryIdx), s) in zip(matches, status):
            # only process the match if the keypoint was successfully
            # matched
            if s is not None:
                new_h_a += int(kps_a[queryIdx][1])
                new_w_a += int(kps_a[queryIdx][0])
                new_h_b += int(kps_b[trainIdx][1])
                new_w_b += int(kps_b[trainIdx][0])
                true_matches += 1

        new_h_a /= true_matches
        new_w_a /= true_matches
        new_h_b /= true_matches
        new_w_b /= true_matches

        result_ = np.zeros(((new_h_b - new_h_a + h_a), w_a, 3), np.uint8)

        result_[:new_h_b, :w_b, :] = image_b[:new_h_b, :w_b, :]

        if new_w_b < new_w_a:
            result_[new_h_b:, :w_a - new_w_a + new_w_b] = image_a[new_h_a:, new_w_a - new_w_b:w_b]
        else:
            result_[new_h_b:, new_w_b - new_w_a:w_a] = image_a[new_h_a:, :w_a + new_w_a - new_w_b]

        # result_ = cv2.warpAffine(image_a, h, (w_a, (new_h_b - new_h_a + h_a)))
        # result_[0:new_h_b, 0:w_b] = image_b[0:new_h_b, :]

        # check to see if the keypoint matches should be visualized
        if show_matches:
            vis_ = self.draw_matches(image_a, image_b, kps_a, kps_b, matches, status)
            # return a tuple of the stitched image and the
            # visualization
            return result_, vis_, err
        # return the stitched image
        return result_, None, err

    def detect_and_describe(self, image):

        # convert the image to grayscale
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # check to see if we are using OpenCV 3.X
        if self.isv3:
            # detect and extract features from the image
            descriptor = cv2.xfeatures2d.SIFT_create()
            (kps, features) = descriptor.detectAndCompute(image, None)

        # otherwise, we are using OpenCV 2.4.X
        else:
            # detect keypoints in the image
            detector = cv2.FeatureDetector_create("SIFT")
            kps = detector.detect(gray)

            # extract features from the image
            extractor = cv2.DescriptorExtractor_create("SIFT")
            (kps, features) = extractor.compute(gray, kps)

        # convert the keypoints from KeyPoint objects to NumPy
        # arrays
        kps = np.float32([kp.pt for kp in kps])

        # return a tuple of keypoints and features
        return kps, features

    @staticmethod
    def ransac(pts_a, pts_b):

        # here "x" means "width" direction is first element of pts_a/pts_b[i][0]
        # y means height direciton is the second element of pts_a/pts_b[i][1]
        # convert pts_a -> pts_b, that is: image_a -> image_b
        # delta_x = delta_y = delta = 0
        min_sum_delta = 10000
        coefs = np.float32([[0, 0, 0], [0, 0, 0]])
        proper_idxs = np.zeros(2, np.uint8)

        for i in range(0, len(pts_a) - 1):
            for j in range(i + 1, len(pts_a)):
                sum_delta = 0

                m11 = float((pts_b[i][0] - pts_b[j][0]) / (pts_a[i][0] - pts_a[j][0]))
                m12 = pts_b[i][0] - m11 * pts_a[i][0]
                m21 = float((pts_b[i][1] - pts_b[j][1]) / (pts_a[i][1] - pts_a[j][1]))
                m22 = pts_b[j][1] - m21 * pts_a[j][1]

                for k in range(0, len(pts_a)):

                    if k == i or k == j:
                        continue

                    new_x = m11 * pts_a[k][0] + m12
                    new_y = m21 * pts_a[k][1] + m22

                    delta_x = new_x - pts_b[k][0]
                    delta_y = new_y - pts_b[k][0]
                    delta = math.sqrt(math.pow(delta_x, 2) + math.pow(delta_y, 2))

                    sum_delta += delta

                if sum_delta < min_sum_delta:
                    min_sum_delta = sum_delta
                    coefs = np.float32([[m11, 0.0, m12], [0.0, m21, m22]])
                    # proper_idxs = np.zeros(2, np.float)
                    proper_idxs = [i, j]

        idxs = np.zeros(len(pts_a), np.uint)
        idxs[proper_idxs[0]] = idxs[proper_idxs[1]] = 1
        return coefs, idxs, min_sum_delta / len(pts_a)

    # @staticmethod
    def match_keypoints_ransac(self, kps_a, kps_b, features_a, features_b, ratio):

        # compute the raw matches and initialize the list of actual
        # matches
        matcher = cv2.DescriptorMatcher_create("BruteForce")
        raw_matches = matcher.knnMatch(features_a, features_b, 2)
        matches = []

        # loop over the raw matches
        for m in raw_matches:
            # ensure the distance is within a certain ratio of each
            # other (i.e. Lowe's ratio test)
            if not (len(m) == 2 and (m[0].distance < m[1].distance * ratio)):
                continue
            if not (math.fabs(kps_b[m[0].trainIdx][0] - kps_a[m[0].queryIdx][0]) < int(
                                self.width * self.width_thresh_ratio / 100)):
                continue
            if not (math.fabs(math.fabs(self.height_b - kps_b[m[0].trainIdx][1] + kps_a[m[0].queryIdx][
                    1]) - self.height_a / 2) < self.height_a * self.height_thresh_ratio / 100):
                continue

            matches.append((m[0].trainIdx, m[0].queryIdx))

        # computing a homography requires at least 4 matches
        if len(matches) > 4:
            # construct the two sets of points
            pts_a = np.float32([kps_a[i] for (_, i) in matches])
            pts_b = np.float32([kps_b[i] for (i, _) in matches])

            # compute the homography between the two sets of points using RANSAC
            h, status, err = self.ransac(pts_a, pts_b)
            # (h, status) = cv2.findHomography(pts_a, pts_b, cv2.RANSAC, 5.0)

            # return the matches along with the homograpy matrix
            # and status of each matched point
            return matches, h, status, err

        else:
            shift_x = shift_y = 0
            status = np.zeros(len(matches), np.uint8)
            for i in range(0, len(matches)):
                shift_x += (kps_b[matches[0][0]][0] - kps_a[matches[0][1]][0]) / len(matches)
                shift_y += (kps_b[matches[0][0]][1] - kps_a[matches[0][1]][1]) / len(matches)
                status[i] = 1

            h = np.float32([[1, 0, shift_x], [0, 1, shift_y]])

            return matches, h, status, None

            # otherwise, no homograpy could be computed

    @staticmethod
    def draw_matches(image_a, image_b, kps_a, kps_b, matches, status):
        # initialize the output visualization image
        (h_a, w_a) = image_a.shape[:2]
        (h_b, w_b) = image_b.shape[:2]
        vis_ = np.zeros((max(h_a, h_b), (w_a + w_b), 3), dtype="uint8")
        vis_[0:h_a, 0:w_a] = image_a
        vis_[0: h_b, w_a:w_a + w_b] = image_b

        for ((trainIdx, queryIdx), s) in zip(matches, status):
            # draw whole the matches
            pt_a = (int(kps_a[queryIdx][0]), int(kps_a[queryIdx][1]))
            pt_b = (int(kps_b[trainIdx][0]) + w_a, int(kps_b[trainIdx][1]))
            cv2.line(vis_, pt_a, pt_b, (0, 0, 255), 1)

            # only process the match if the keypoint was successfully
            # draw the match
            # matched
            if s == 1:
                cv2.line(vis_, pt_a, pt_b, (0, 255, 0), 1)

        # return the visualization
        return vis_


if __name__ == '__main__':

    s_time = time.time()

    default_width = 200
    default_error_thresh = 30

    imageA = cv2.imread("img/11.jpg")
    imageB = cv2.imread("img/22.jpg")
    imageA = imutils.resize(imageA, width=default_width)
    imageB = imutils.resize(imageB, width=default_width)

    stitcher = Stitcher()

    (result, vis, error) = stitcher.stitch([imageA, imageB], show_matches=True)
    if platform.system() == 'Windows':
        cv2.imshow("1-2 keypoints", vis)
        cv2.imshow("1-2 results", result)

    imageB = cv2.imread("img/33.jpg")
    imageB = imutils.resize(imageB, width=default_width)
    (result, vis, error) = stitcher.stitch([result, imageB], show_matches=True)
    if platform.system() == 'Windows':
        cv2.imshow("1-2-3 keypoints", vis)
        cv2.imshow("1-2-3 results", result)

    imageB = cv2.imread("img/44.jpg")
    imageB = imutils.resize(imageB, width=default_width)
    (result, vis, error) = stitcher.stitch([result, imageB], show_matches=True)

    if platform.system() == 'Windows':
        cv2.imshow("1-2-3-4 keypoints", vis)
        cv2.imshow("1-2-3-4 results", result)
        cv2.waitKey(0)

    print 'Elapsed: ', time.time() - s_time


