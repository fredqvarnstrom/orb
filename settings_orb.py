import os

WINDOW_WIDTH = 1920
WINDOW_HEIGHT = 1080
WINDOW_FULLSCREEN = False

USE_OPENCV = True

INITIAL_SCREEN = 'welcome_screen'

INI_FILE = os.path.expanduser('~/.orb.ini')
EXPORT_FILE = 'store.log'

try:
    from local_settings import *
except ImportError:
    pass
