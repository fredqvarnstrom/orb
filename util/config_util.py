"""
    Utility for configuration
"""
import ConfigParser
import os

import settings_orb

config = ConfigParser.RawConfigParser()
config_file = settings_orb.INI_FILE


def get_config(section, option, default_value=None):
    """
    Get configuration value
    :param section:
    :param option:
    :param default_value:
    :return:
    """
    try:
        config.read(config_file)
        val = config.get(section=section, option=option)
        return val
    except ConfigParser.NoSectionError:
        return default_value
    except ConfigParser.NoOptionError:
        return default_value


def set_config(section, option, value):
    if section not in config.sections():
        config.add_section(section)
    config.set(section, option, value)
    try:
        with open(config_file, 'w') as configfile:
            config.write(configfile)
        return True
    except IOError as e:
        # print('Failed to write default geo-location to file: {}'.format(e))
        return False
