
# For static IP, consult /etc/dhcpcd.conf and 'man dhcpcd.conf'

# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

iface eth0 inet dhcp
    address 192.168.1.106
    gateway 192.168.1.1
    netmask 255.255.255.0
    network 192.168.1.0
    broadcast 192.168.1.255
    
allow-hotplug wlan0
iface wlan0 inet dhcp
    wpa-ssid TP-LINK_REAL3D
    wpa-psk dkanehahffk
allow-hotplug wlan1
iface wlan1 inet manual
