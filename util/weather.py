# coding=utf-8
import datetime
import json
import pprint
import urllib
from urllib2 import urlopen

from util.net import get_lat_loc

yahoo_base_url = "https://query.yahooapis.com/v1/public/yql?"


def get_woeid_by_loc(lat, lon):
    """
    Get Yahoo WOEID from latitude & longitude
    """
    url = "{}q=select%20woeid%20from%20geo.places%20where%20text%3D%" \
          "22({},{})%22%20limit%201&diagnostics=false&format=json".format(yahoo_base_url, lat, lon)
    try:
        response = urlopen(url)
        data = json.load(response)
        return data['query']['results']['place']['woeid']
    except Exception as e:
        print 'Failed to get geolocation, seems like no-internet? - {}'.format(e)


def get_weather_by_loc(lat, lon):
    """
    Get weather from latitude & longitude
    """
    woeid = get_woeid_by_loc(lat, lon)
    if woeid is None:
        return None
    yql_query = "select * from weather.forecast where woeid={}".format(woeid)
    yql_url = yahoo_base_url + urllib.urlencode({'q': yql_query}) + "&format=json"
    try:
        result = urlopen(yql_url).read()
        data = json.loads(result)
        return data['query']['results']['channel']
    except Exception as e:
        print 'Failed to get weather: {}'.format(e)
        pass


def get_weather():
    """
    Retrieve geo location and get weather information
    """
    lat_loc = get_lat_loc()
    if lat_loc is None:
        return None
    lat, loc = lat_loc
    d = get_weather_by_loc(lat, loc)
    try:
        forecasts = [
            {
                'date': datetime.datetime.strptime(f['date'], "%d %b %Y").date(),
                'high': str(f['high']),
                'low': str(f['low']),
                'state': str(f['text'])
            } for f in d['item']['forecast']
        ]
        try:
            today_forecast = [item for item in forecasts if item['date'] == datetime.datetime.now().date()][0]
        except IndexError:
            today_forecast = forecasts[0]
            
        temp_unit = '°' + str(d['units']['temperature'])
        wind_dir = int(d['wind']['direction'])
        wind_dir_str = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'][wind_dir / 45]
        wind_speed = str(d['wind']['speed']) + str(d['units']['speed'])
        data = dict(
            sunrise=str(d['astronomy']['sunrise']),
            sunset=str(d['astronomy']['sunset']),

            humidity=str(d['atmosphere']['humidity']) + '%',
            pressure=str(d['atmosphere']['pressure']) + str(d['units']['pressure']),
            rising=float(d['atmosphere']['rising']),
            visibility=str(d['atmosphere']['visibility']) + str(d['units']['distance']),

            state=str(d['item']['condition']['text']),

            temperature=str(d['item']['condition']['temp']),
            temperature_unit=temp_unit,
            feels_like=str(d['wind']['chill']) + '°',
            temp_high=today_forecast['high'] + '°',
            temp_low=today_forecast['low'] + '°',

            wind='{} {}'.format(wind_speed, wind_dir_str),
        )
        return data
    except (IndexError, KeyError, ValueError):
        pass


if __name__ == '__main__':
    pprint.pprint(get_weather_by_loc(52.382, 46.8995))
    # pprint.pprint(get_weather())
