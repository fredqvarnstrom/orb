import os

from kivy.factory import Factory
from kivy.lang import Builder
from kivy.properties import NumericProperty, ListProperty, StringProperty, AliasProperty
from kivy.uix.boxlayout import BoxLayout
import assets.styles.defaultstyle as style
from .base import AnimatedWidget, ClickableWidget

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'card.kv'))


class BorderLessCard(BoxLayout, AnimatedWidget, ClickableWidget):
    border_radius = NumericProperty(0)
    background_color = ListProperty(style.BACKGROUND_COLOR_LIGHT)
    orientation = StringProperty('vertical')

    def _get_diameter(self):
        return self.border_radius * 2

    _diameter = AliasProperty(_get_diameter, None, bind=('border_radius',))


class Card(BorderLessCard):
    border_color = ListProperty([0.48, 0.55, 0.59, 0.5])
    border_width = NumericProperty(1)


class TitledCard(BorderLessCard):
    border_color = ListProperty([0.48, 0.55, 0.59, 0.5])
    border_width = NumericProperty(1)
    title = StringProperty('')

    def __init__(self, **kwargs):
        super(TitledCard, self).__init__(**kwargs)


Factory.register('Card', cls=Card)
Factory.register('TitledCard', cls=TitledCard)
