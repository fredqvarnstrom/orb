import os
import threading
import time
from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, BooleanProperty, ObjectProperty
from kivy.uix.modalview import ModalView

import util.net
from kivy.uix.popup import Popup
from kivymd.dialog import MDDialog
import util.net
import util.config_util
from functools import partial

from os.path import isdir

from os.path import join

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'dialog.kv'))


class WiFiInfoDialog(MDDialog):

    ssid = StringProperty('')
    quality = NumericProperty(0)
    ip = StringProperty('')
    mac_addr = StringProperty('')

    def __init__(self, **kwargs):
        super(WiFiInfoDialog, self).__init__(**kwargs)

    def on_open(self):
        Clock.schedule_once(self.get_wifi_details)

    def get_wifi_details(self, *args):
        wifi = util.net.get_detail_of_connected_net()
        self.ssid = wifi['ssid']
        self.quality = int(wifi['quality'])
        self.ip = wifi['ip']
        self.mac_addr = wifi['mac']


class WiFiConnectDialog(MDDialog):

    ssid = StringProperty('')
    pwd = StringProperty('')
    loading_dlg = ObjectProperty(None)

    def __init__(self, **kwargs):
        self.register_event_type('on_done')
        super(WiFiConnectDialog, self).__init__(**kwargs)
        self.loading_dlg = LoadingDialog()

    def on_done(self, *args):
        pass

    def on_yes(self, *args):
        self.pwd = self.ids.txt_pwd.text
        Clock.schedule_once(self.show_load)
        threading.Thread(target=self.connect_to_ap).start()

    def show_load(self, *args):
        # self.ids.btn_yes.disabled = True
        # self.ids.btn_no.disabled = True
        # self.ids.txt_pwd.disabled = True
        # self.ids.spinner.opacity = 1
        self.dismiss()
        self.loading_dlg.open()

    @mainthread
    def hide_load(self):
        # self.ids.btn_yes.disabled = False
        # self.ids.btn_no.disabled = False
        # self.ids.txt_pwd.disabled = False
        # self.ids.spinner.opacity = 0
        self.loading_dlg.dismiss()

    def connect_to_ap(self, *args):
        ip = util.net.connect_to_ap('wlan0', self.ssid, self.pwd)
        Clock.schedule_once(partial(self.connect_callback, ip))

    def connect_callback(self, ip, *args):
        self.hide_load()
        if ip is None:
            NotificationDialog(message='Failed to connected to {}, please try again later'.format(self.ssid)).open()
            # self.ids.container.add_widget(
            #     MDLabel(font_style='Body1', theme_text_color='Error', text="Failed to connect", halign='center'))
        else:
            NotificationDialog(message='Connected to {}, new IP: {}'.format(self.ssid, ip)).open()
            self.dispatch('on_done', ip)


class AuthDialog(MDDialog):
    pwd = StringProperty('')
    mode = StringProperty('')
    is_error = BooleanProperty(False)

    def __init__(self, **kwargs):
        self.register_event_type('on_success')
        super(AuthDialog, self).__init__(**kwargs)
        self.pwd = util.config_util.get_config('admin', 'password_{}'.format(kwargs['mode']), '')

    def on_open(self):
        self.clear_error()
        self.ids.txt_pwd.text = ''
        self.ids.txt_pwd.focus = True

    def on_success(self, *args):
        pass

    def on_ok(self):
        if self.ids.txt_pwd.text == self.pwd:
            self.dismiss()
            self.dispatch('on_success', self.mode)
        else:
            self.is_error = True

    def clear_error(self):
        self.is_error = False

    def on_touch_down(self, touch):
        if self.is_error:
            self.ids.txt_pwd.text = ''
            self.is_error = False
        super(AuthDialog, self).on_touch_down(touch)


class LoadingDialog(ModalView):
    pass


class NotificationDialog(MDDialog):

    message = StringProperty('')

    def __init__(self, **kwargs):
        super(NotificationDialog, self).__init__(**kwargs)


class YesNoDialog(MDDialog):

    message = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(YesNoDialog, self).__init__(**kwargs)

    def on_yes(self):
        self.dispatch('on_confirm')

    def on_confirm(self):
        pass


class FolderChooserDialog(Popup):

    path = StringProperty('')

    def __init__(self, **kwargs):
        super(FolderChooserDialog, self).__init__(**kwargs)
        self.register_event_type('on_confirm')

    def is_dir(self, directory, filename):
        return isdir(join(directory, filename))

    def on_ok(self):
        self.dismiss()
        self.dispatch('on_confirm', self.ids.txt_path.text)

    def on_confirm(self, *args):
        pass

    def selected(self):
        self.ids.txt_path.text = str(self.ids.fc.selection[0])
