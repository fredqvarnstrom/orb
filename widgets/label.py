import os

from kivy.factory import Factory
from kivy.lang import Builder
from kivy.uix.label import Label

from widgets.base import AnimatedWidget

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'label.kv'))


class ColoredLabel(Label, AnimatedWidget):

    def __init__(self, **kwargs):
        super(ColoredLabel, self).__init__(**kwargs)
        self.register_event_type('on_press')

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            self.dispatch('on_press')

    def on_press(self):
        pass


class H0(ColoredLabel):
    pass


class H1(ColoredLabel):
    pass


class H2(ColoredLabel):
    pass


class H3(ColoredLabel):
    pass


class H4(ColoredLabel):
    pass


class H5(ColoredLabel):
    pass


class H6(ColoredLabel):
    pass


class P(ColoredLabel):
    pass


Factory.register('H0', cls=H0)
Factory.register('H1', cls=H1)
Factory.register('H2', cls=H2)
Factory.register('H3', cls=H3)
Factory.register('H4', cls=H4)
Factory.register('H5', cls=H4)
Factory.register('H6', cls=H6)
Factory.register('P', cls=P)
