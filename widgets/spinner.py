import os
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.factory import Factory
from kivy.properties import StringProperty, DictProperty, ListProperty
from kivy.uix.boxlayout import BoxLayout
from .base import DefaultInput, LabeledInputBase, AnimatedWidget

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'spinner.kv'))


class ORBSpinner(BoxLayout, DefaultInput, AnimatedWidget):
    """
    If we inherit from kivy.uix.spinner.Spinner it causes problems with window size,
    also window will not run in fullscreen.
    """

    def custom_error_check(self):
        pass

    values = DictProperty({})

    order = ListProperty([])
    """Set custom choices order
    """

    def __init__(self, *args, **kwargs):
        self.register_event_type('on_change')
        super(ORBSpinner, self).__init__(*args, **kwargs)
        self.bind(values=self._update_spinner_values)
        self.bind(order=self._update_spinner_values)
        Clock.schedule_once(self.bind_spinner)

    def bind_spinner(self, *args):
        self.ids.spinner._dropdown.bind(on_select=self.on_change_spinner)

    def _update_spinner_values(self, obj, value):
        keys = self.order if self.order else self.values.keys()
        if not self.order:
            keys = sorted(keys)
        values = (self.values[k] for k in keys)
        self.ids.spinner.values = values

    def _get_value(self):
        val = self.ids.spinner.text
        for key, value in self.values.items():
            if value == val:
                return key

        return None

    def on_touch_down(self, touch):
        if self.collide_point(touch.x, touch.y):
            self.clear_errors()
        super(ORBSpinner, self).on_touch_down(touch)

    def set_value(self, value):
        self.values = value

    def update_current_value(self, value):
        self.ids.spinner.text = value

    def set_values(self, values):
        if type(values) == list:
            new_val = {}
            for val in values:
                new_val[val] = val
            values = new_val
        self.values = values

    def on_change_spinner(self, *args):
        self.ids.spinner._on_dropdown_select(*args)
        self.dispatch('on_change')

    def on_change(self):
        pass


class LabeledSpinner(LabeledInputBase):
    def custom_error_check(self):
        pass

    values = DictProperty({})
    order = ListProperty([])

    hint_text = StringProperty('')
    password = False

    def __init__(self, **kwargs):
        self.register_event_type('on_change')
        super(LabeledSpinner, self).__init__(**kwargs)
        self.bind(values=self._update_spinner_values)

    def on_order(self, obj, val):
        self.ids.input.order = self.order

    def _update_spinner_values(self, obj, value):
        self.ids.input.set_value(value)

    def set_value(self, value):
        if type(value) == str:
            self.ids.input.update_current_value(value)
        else:
            self.values = value

    def on_change_spinner(self):
        self.dispatch('on_change')

    def on_change(self):
        pass


Factory.register('ORBSpinner', cls=ORBSpinner)
Factory.register('LabeledSpinner', cls=LabeledSpinner)
