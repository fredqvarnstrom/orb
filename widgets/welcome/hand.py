import os
from kivy.animation import Animation
from kivy.lang import Builder
from kivy.properties import ListProperty, NumericProperty, AliasProperty
from widgets.base import AnimatedWidget


Builder.load_file(os.path.join(os.path.dirname(__file__), 'hand.kv'))


class HandAnimatedImage(AnimatedWidget):
    size_scale_factor = NumericProperty(1.1)

    max_circle_radius = NumericProperty(20)

    circle_color = ListProperty([0.47, 0.56, 0.61, 1])

    ratio = NumericProperty(0.82)

    duration = NumericProperty(.4)

    _circle_radius = NumericProperty(1)

    _scale = NumericProperty(1.1)

    _circle_opacity = NumericProperty(0)

    def _get_cirlce_center(self):
        return [self.pos[0] + self.size[1] * self.ratio / 7.1, self.pos[1] + self.size[1] / 1.12]

    _circle_center = AliasProperty(_get_cirlce_center, None, bind=('size', 'pos'))

    def __init__(self, **kwargs):
        super(HandAnimatedImage, self).__init__(**kwargs)
        self.hand_up = None
        self.hand_down = None
        self.pulse = None
        self.create_animation()
        self.start_animation()

    def create_animation(self):
        self.hand_up = Animation(_scale=self.size_scale_factor, t='out_quad', d=self.duration)
        self.hand_down = Animation(_scale=1, t='in_quad', d=self.duration)
        self.hand_up.bind(on_complete=self.on_hand_up)
        self.hand_down.bind(on_complete=self.on_hand_down)

        circle_anim_appear_radius = \
            Animation(_circle_radius=self.max_circle_radius, d=self.duration)
        circle_anim_appear_opacity = \
            Animation(_circle_opacity=1, d=0) + \
            Animation(_circle_opacity=0, d=self.duration)
        circle_anim_appear = circle_anim_appear_radius & circle_anim_appear_opacity
        circle_anim_hide = \
            Animation(_circle_radius=4, d=self.duration)

        self.pulse = circle_anim_appear + circle_anim_hide

    def start_animation(self):
        self.hand_up.start(self)

    def on_hand_up(self, *args):
        self.hand_down.start(self)

    def on_hand_down(self, *args):
        self.hand_up.start(self)
        self.pulse.start(self)
