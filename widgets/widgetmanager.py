"""
Import common widgets here to register them and use in the kv files.
Do not import screen specific widgets.
"""
import widgets.button
import widgets.label
import widgets.card
import widgets.checkbox
import widgets.spinner
import widgets.input
import widgets.wifi_icon
