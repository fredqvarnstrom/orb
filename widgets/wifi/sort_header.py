import os
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, BooleanProperty
from kivy.uix.boxlayout import BoxLayout


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'sort_header.kv'))


class SortHeader(BoxLayout):

    text = StringProperty('')
    active = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(SortHeader, self).__init__(**kwargs)
        self.register_event_type('on_press')

    def on_touch_down(self, touch):
        super(SortHeader, self).on_touch_down(touch)
        if self.collide_point(*touch.pos):
            self.ids.img_sort.opacity = 1
            self.dispatch('on_press')

    def on_press(self):
        pass

    def disable_sort(self):
        self.ids.img_sort.opacity = 0


if __name__ == '__main__':
    from kivy.base import runTouchApp
    runTouchApp(SortHeader())
